require('dotenv').config();
const cors = require('cors');
const express = require("express");
const path = require("path");
const twitterProxy = require('./middleware/twitterProxy');

const app = express();
const PORT = 3001;

// middleware
app.use('/', cors(), twitterProxy());

// serve static files
app.use(express.static(path.join(__dirname, "..", "build")));
app.use(express.static("public"));

// start express server
app.listen(PORT, () => {
  console.log(`server started on port ${PORT}`);
});
