const fetch = require('node-fetch');

// proxy twitter requests
module.exports = () => (proxyReq, proxyRes) => {

  fetch(`https://api.twitter.com/2${proxyReq.originalUrl}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${process.env.AUTH}`
    }
  })
  .then(res => {
    if (res.status === 429) {
      const rateLimitReset = res.headers.get("x-rate-limit-reset");
      const currTime = Math.floor(new Date().getTime() / 1000);
      const minutes = Math.ceil((rateLimitReset - currTime) / 60);

      return {
        errors: [{
          status: res.status, 
          detail: res.statusText,
          message: `Try again in ${minutes} minutes.`
        }]
      }
    }
    return res.json()
  })
  .then(res => proxyRes.json(res))
  .catch(err => err)
};
