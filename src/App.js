import React, { useEffect, useState } from 'react';
import { User } from './components/User';
import { Error } from './components/Error';
import { Followers } from './components/Followers';
import { Input } from './components/Input';


const App = (props) => {
  const { max, defaultParams } = props;

  // state
  const [query, setQuery] = useState("");
  const [user, setUser] = useState({});
  const [error, setError] = useState({});
  const [followers, setFollowers] = useState([]);
  const [paginationToken, setPaginationToken] = useState("");

  // event listeners
  useEffect(() => {
    document.addEventListener('scroll', handleLazyLoad);
    return () => {
      document.removeEventListener('scroll', handleLazyLoad);
    }
  });



  // handles all requests to twitter endpoint
  // proxied through express server bc of CORS issues
  function getData({ path, params }) {
    let url = makeUrl({ path, params });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        // on data response
        if (res.data) {
          if (res.data.username) {
            onUserResponse(res)
          } else if (res.data.length) {
            onFollowerResponse(res)
          }
        }
        // on error response
        if (res.errors) {
          onError(res.errors[0])
        }
      })
      .catch(error => onError({ detail: error }))
  };


  // handle user response and update state
  function onUserResponse(res) {
    if (res.data.username) {
      setUser(res.data);
      setError({});
    }
  };


  // handle followers response
  function onFollowerResponse(res) {
    if (res.data.length) {
      setPaginationToken(res.meta.next_token || "");
      setFollowers(followers.concat(res.data));
    }
  };


  // handle errors
  function onError(error) {
    setError({
      error: `${error.status || ""} ${error.detail || error}`,
      description: error.message
    });

    // don't remove followers if rate limit error
    if (error.status !== 429) {
      setUser({});
      setFollowers([]);
    }
  };


  // format url and add params
  function makeUrl({ path, params }) {
    let url = path;
    url = params ? url+"?"+params : url;
    url = params ? url+"&" : url+"?";
    
    return url + defaultParams;
  }


  // lazy load more followers on scrollY position 0
  function handleLazyLoad() {
    const scrollY = window.scrollY;
    const windowHeight = window.innerHeight;
    const offSetHeight = document.body.offsetHeight;

    if (offSetHeight - windowHeight - scrollY <= 0) {
      if (paginationToken && followers.length) {
        getData({
          path:`/users/${user.id}/followers`,
          params: `max_results=${max}&pagination_token=${paginationToken}`
        });
      }
    }
  };


  // queries for user on each new input
  function onInputChange(e) {
    const currValue = e.target.value;
    
    setQuery(e.target.value);
    setFollowers([]);

    // enable btn on query change
    const button = document.querySelector('button');
    if (button) button.disabled = false;

    // only query if value in search bar
    if (currValue.length > 0) {
      getData({path: `/users/by/username/${e.target.value}`});
    } else {
      setQuery("");
      setError({});
      setUser({});
    }
  };

  
	return (
    <main className="App">
      <Input 
        onInputChange={onInputChange} 
        value={query} 
      />      
      <User
        user={user}
        max={max} 
        getData={getData}
      />
      <Followers followers={followers} />
      <Error {...error} />
    </main>
	)
};


App.defaultProps = {
  max: 50,
  defaultParams: "user.fields=profile_image_url,verified"
};

export default App;
