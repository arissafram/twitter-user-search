import React from 'react';
import PropTypes from 'prop-types';


export const Error = ({ error, description }) => {
  if (!error) return null;

  return (
    <section className="Error"> 
      <p className="Error__message">{error}</p>
      {description && (
        <p className="Error__description">
          {description}
        </p>
      )}
    </section>
  )
};

Error.propTypes = {
  error: PropTypes.object,
	description: PropTypes.string
};
