import React, { useRef } from 'react';
import PropTypes from 'prop-types';

export const Input = ({ onInputChange, query }) => {

  const inputContainer = useRef(null);

  // change size of input container
  function handleTransition(e) {
    const inputContainerClasses = inputContainer.current.classList;
    const body = document.querySelector('body');
      
    if (e.target.value.length > 0) {
      if (!inputContainerClasses.contains('search-exists')) {
        inputContainerClasses.add('search-exists');
        body.classList.add('search-exists');
        body.classList.add('search-empty');
      }
    } else {
      inputContainerClasses.remove('search-exists');
      body.classList.remove('search-exists');
      body.classList.add('search-empty');
    }
  };

  return (
    <section ref={inputContainer} className="Input">
      <span className="Input__twitter-icon" />
      <input
        id="search"
        type="text"
        className="Input__field"
        value={query}
        onChange={onInputChange}
        onKeyUp={handleTransition}
        maxLength="15"
        placeholder="Search for a User"
        autoComplete="off"
      />
    </section>
  )
};

Input.propTypes = {
  query: PropTypes.string,
	onInputChange: PropTypes.func
};
