import React from 'react';
import { User } from './User';
import PropTypes from 'prop-types';

export const Followers = ({ followers }) => {
  
  if (!followers.length) return null;

  return (
    <section className="Followers">
      {followers.map(item => (
        <User 
          key={item.id} 
          className="Follower" 
          user={item} 
        />
      ))}
    </section>
  )
};

Followers.propTypes = {
  followers: PropTypes.array,
};
