import React from 'react';
import verifiedIcon from '../assets/verified.svg';
import PropTypes from 'prop-types';


export const User = ({ user, max, getData }) => {
  if (!user.username) return null;

  const followerButtonEl = (
    <li>
      <button
        id={user.id}
        className="User__info--button"
        onClick={(e) => {
          getData({
            path: `/users/${user.id}/followers`,
            params: `max_results=${max}`
          })
          e.target.disabled = true
        }}
      >
        View Followers
      </button> 
    </li>
  );

  const verifiedEl = user.verified && (
    <img 
      className="User__info--icon" 
      src={verifiedIcon} 
      alt="verified"
    />
  );

  return (
    <section className="User">
      <img 
        alt={user.name}
        className="User__image"
        src={user.profile_image_url} 
        height="40px"
        width="40px"
      />
      <ul className="User__info">
        <li className="User__info--name-container">
          <span className="User__info--name">
            {user.name}
          </span>
          {verifiedEl}
        </li>
        <li className="User__info--id">{user.id}</li>
        {getData && followerButtonEl}
      </ul>
    </section>
  )
};

User.propTypes = {
  max: PropTypes.number,
	user: PropTypes.object,
	getData: PropTypes.func
};
