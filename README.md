# Twitter User Search

**Setup / Install:**<br>
`git clone` this repo<br>
add a `.env` file: `AUTH=[Bearer Token from your Twitter developer portal]`<br>
run `npm install` inside the root directory<br>
run `npm start`<br>
app runs at `localhost:3000`
<br><br>

**Technologies:**<br>
Twitter API v2, React, Express, Sass
<br><br>

**App Layout:**<br>
```
<App>
  <Input />
  <User />
  <Followers>
    <User />
  </Followers>
  <Error />
</App>
```
<br>

**Notes:**<br>
When the page loads, you can search for a user. The input container will minimize on keyUp revealing a user or an error message. Whenever the input is empty, the input container will expand again.

If a user exists, you can click the 'View Followers' button to see their followers. Scrolling to the bottom of the page will trigger the next set of 50 followers. Pagination is triggered at scrollY equals 0, though it should probably be triggered around 200px.

The input allows a max of 15 characters, which is Twitter's username character limit. I didn't add an error message here, just a maxLength on the input.

The styles are weak. Certainly not mobile first. Particularly unhappy with my use of `overflow-wrap: anywhere`, but that's for a future fix.